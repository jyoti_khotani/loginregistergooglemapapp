import 'package:flutter/material.dart';
import 'package:flutter_appentus_test_app/service/WebApiImplemention.dart';
import 'author.dart';

class AuthorListViewModel with ChangeNotifier{
  bool isLoading = true;
  List<Author> authorList = List<Author>();
  String message;

  Future<void> fetchCategories() async{
    try{
      final results= await WebApiImplemention().fetchAllAuthorList();
      this.authorList = results;
      isLoading = false;
      // print("my name is ${results}");
      notifyListeners();
    }catch(error){
      isLoading = false;
      message = error.toString();
      notifyListeners();
    }
  }
}