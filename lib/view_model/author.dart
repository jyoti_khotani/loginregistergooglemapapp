import 'package:flutter/cupertino.dart';

class Author with ChangeNotifier{
  final String id;
  final String author;
  final int width;
  final int height;
  final String url;
  final String download_url;

  Author({@required this.id,
          @required this.author,
            @required this.width,
            @required this.height,
            @required this.url,
            @required this.download_url,});


  factory Author.fromJson(dynamic json) {
    return Author(
        id: json["id"] ,
        author: json["author"],
        width: json["width"] as int,
        height: json["height"] as int,
        url: json["url"],
        download_url: json["download_url"]
    );
  }

}