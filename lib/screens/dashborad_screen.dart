import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_appentus_test_app/screens/google_map_with_current_location_screen.dart';
import 'package:flutter_appentus_test_app/screens/registration_screen.dart';
import '../common.dart';

class DashBoardScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    void navigateToScreen(String routeValue){
      Navigator.pushReplacementNamed(context, routeValue);
    }
    return Scaffold(
        appBar: AppBar(
          title: Text("Registration"),
        ),
      body: Center(
        child: Container(
          height: 250,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                onTap:(){
                  navigateToScreen(RegistrationScreen.routName);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(10.0)
                  ),
                  padding: EdgeInsets.only(left:25.0, right: 25.0, top: 15.0, bottom: 15.0),
                  child: Text("Signup",
                    style: sttyleText(
                        color: Colors.white,weight: FontWeight.w400,size: 15),),
                ),
              ),
              InkWell(
                onTap:(){
                  navigateToScreen(GoogleMapWithCurrentLocationScreen.routName);
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(10.0)
                  ),
                  padding: EdgeInsets.all(15.0),
                  child: Text("Google Map",
                    style: sttyleText(
                        color: Colors.white,weight: FontWeight.w400,size: 15),),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
