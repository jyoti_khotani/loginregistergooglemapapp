import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_appentus_test_app/screens/google_map_with_current_location_screen.dart';
import 'package:flutter_appentus_test_app/screens/login_screen.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../common.dart';
import '../constants.dart';
import '../database_helper/database_provider.dart';

class RegistrationScreen extends StatefulWidget {
  static final String routName= 'RegistrationScreen';
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  TextEditingController confrimpasswordController = TextEditingController();
  final databaseprovider = DataBaseprovider.createInstance();
  File _image;
  String imagePath="";
//Open camera
  _imgFromCamera() async {
    File image = (await ImagePicker.pickImage(source: ImageSource.camera,imageQuality: 50)) as File;
    setState(() {
      _image = image;
    });
  }
  // open gallery
   _imgFromGallery() async {
    File image = await  ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);
      setState(() {
      _image = image;
      imagePath = image.path;
      print("get image file:::${_image}");
      print("print line 38:::: ${imagePath}");
      });
    }
    Future<void> _signUpUser() async{
      if (nameController.text.isEmpty) {
        Fluttertoast.showToast(msg: 'Please enter name');
        return;
      }
      else if (!RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(emailController.text)) {
        Fluttertoast.showToast(msg: 'Please enter correct email');
        return;
      }
     else if (emailController.text.isEmpty) {
        Fluttertoast.showToast(msg: 'Please enter mobile email');
        return;
      }
     else if(numberController.text.isEmpty){
        Fluttertoast.showToast(msg: 'Please enter number');
        return;
      }
    else if(passwordController.text.isEmpty){
        Fluttertoast.showToast(msg: 'Please enter password');
        return;
      }
    else if(imagePath.isEmpty){
        Fluttertoast.showToast(msg: 'Please choose a picture');
        return;
      }else{
        AdduserToLocal(nameController.text, emailController.text, numberController.text, passwordController.text);
      }

    }

  Future<bool> AdduserToLocal(String userName, String userEmail, String userNumber, String userPassword) async {
    try {
      await databaseprovider.initializeDatabase();
      int result=await databaseprovider.insertUserToLocal(userName, userEmail, userNumber,userPassword);
      if(result!=0){
        print(" registerd added");
        Fluttertoast.showToast(msg: 'Registered Successfuly');
        nameController.clear();
        numberController.clear();
        emailController.clear();
        passwordController.clear();
        var prefs = await SharedPreferences.getInstance();
        prefs.setString(username, userName);
      }
      return true;
    } catch (Exception) {
      return false;
    }
  }
  Future<void> checkUserLoginStatus() async {
    var prefs = await SharedPreferences.getInstance();
    bool isLoggedIn = prefs.getBool(loginStatus);
    if(isLoggedIn){
      Navigator.pushReplacementNamed(context, GoogleMapWithCurrentLocationScreen.routName);
    }else{
      print("not login");
    }
  }
  @override
  void initState() {
    super.initState();
    checkUserLoginStatus();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Registration"),
      ),
      body:SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: 15.0, right: 15.0),
            child: Column(
              children: [
               SizedBox(
                height: 20,
                ),
                InkWell(
                  onTap: _imgFromGallery,
                  child: CircleAvatar(
                    radius: 55,
                    backgroundColor: Color(0xffFDCF09),
                    child: _image != null
                        ? ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: Image.file(
                        _image,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                      ),
                    ) : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(50)),
                      width: 100,
                      height: 100,
                      child: Icon(
                        Icons.camera_alt,
                        color: Colors.grey[800],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: lighBlueBackground,
                      borderRadius: BorderRadius.circular(15.0)
                  ),
                  child: TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                        hintText: "Your full name",
                        border: InputBorder.none,
                        hintStyle: sttyleText(
                            color: Colors.black38,weight: FontWeight.w400,size: 15)),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10,),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: lighBlueBackground,
                      borderRadius: BorderRadius.circular(15.0)
                  ),
                  child: TextField(
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        hintText: "Your email",
                        border: InputBorder.none,
                        hintStyle: sttyleText(
                            color: Colors.black38,weight: FontWeight.w400,size: 15)),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10,),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: lighBlueBackground,
                      borderRadius: BorderRadius.circular(15.0)
                  ),
                  child: TextField(
                    controller: numberController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        hintText: "Your number",
                        border: InputBorder.none,
                        hintStyle: sttyleText(
                            color: Colors.black38,weight: FontWeight.w400,size: 15)),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10,),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: lighBlueBackground,
                      borderRadius: BorderRadius.circular(15.0)
                  ),
                  child: TextField(
                    controller: passwordController,
                    decoration: InputDecoration(
                        hintText: "Your password",
                        border: InputBorder.none,
                        hintStyle: sttyleText(
                            color: Colors.black38,weight: FontWeight.w400,size: 15)),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10,),
                ),
                SizedBox(
                  height: 20,
                ),
                FlatButton(
                    onPressed: _signUpUser,
                    child: Padding(
                      padding: const EdgeInsets.only(left:60.0,right: 60.0,top: 15.0,bottom: 15.0),
                      child: Text("Sign up",
                        style: sttyleText(color: Colors.white,
                            weight: FontWeight.w500,
                            size: 17),),
                    ),
                    splashColor: Colors.blueGrey,
                    color: Colors.blue,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(
                            color: Colors.white
                        )
                    )
                ), SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: (){
                    Navigator.pushNamed(context, LoginScreen.routName);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: lighBlueBackground,
                        borderRadius: BorderRadius.circular(15.0)
                    ),
                    child: Text(
                    "Already have account  Login",
                        style: sttyleText(color: Colors.blue,
                            weight: FontWeight.w500,
                            size: 17)
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 10,),
                  ),
                )
              ],

            ),
          ),
        ),
      ),

    );
  }
    // TODO: implement build

  }
