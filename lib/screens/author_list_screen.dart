import 'package:flutter/material.dart';
import 'package:flutter_appentus_test_app/screens/author_item.dart';
import 'package:flutter_appentus_test_app/view_model/author_list_view_model.dart';
import 'package:provider/provider.dart';

class AuthorListScreen extends StatefulWidget {
  static final String routName= 'AuthorListScreen';
  @override
  _AuthorListScreenState createState() => _AuthorListScreenState();
}
class _AuthorListScreenState extends State<AuthorListScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<AuthorListViewModel>(context, listen: false).fetchCategories();
  }
  @override
  Widget build(BuildContext context) {
    final authorData = Provider.of<AuthorListViewModel>(context);
    final authorList=authorData.authorList;
    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
            children: [
              AppBar(title: Text("Author List"),),
              Consumer<AuthorListViewModel>(
                  builder: (context, authorObjectModel, child){
                    if(authorObjectModel.isLoading){
                      return CircularProgressIndicator();
                    }
                    if(authorObjectModel.message!=null){
                      print(authorObjectModel.message);
                      return Center(
                        child: Text(authorObjectModel.message),
                      );
                    }  if (authorObjectModel.authorList == null) {
                      return Center(
                        child: Text("List is empty"),
                      );
                    }
                    return  GridView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                          childAspectRatio: 1.2
                      ),
                      itemCount: authorList.length,
                      itemBuilder: (ctx, index)=>
                          ChangeNotifierProvider.value(
                              value: authorList[index],
                              child: Padding(
                                padding: const EdgeInsets.only(top:5.0,left: 15.0, right: 15.0,bottom: 8.0),
                                child: AuthorItem(),
                              )),

                    );

                  })

            ],
          )


      ) ,
    );

  }
}
