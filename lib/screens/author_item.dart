import 'package:flutter/material.dart';
import 'package:flutter_appentus_test_app/view_model/author.dart';
import 'package:provider/provider.dart';
import '../common.dart';

class AuthorItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authorObj= Provider.of<Author>(context);
    return  Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: kShadowColor,
            spreadRadius: 3,
            blurRadius: 3,
            offset: Offset(10.0, 0.0,), // changes position of shadow
          ),
        ],
      ),

      // padding: EdgeInsets.all(8.0),

      child: Container(
        child: Column(
          children: [
            Expanded(
              child: FittedBox(
                fit: BoxFit.fill,
                child: Image.network(authorObj.download_url,
                        height: 200,
                          width: 200,
                ),
                ),
              ),
            Container(
              decoration: BoxDecoration(
                color: lighBlueforName,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              padding: EdgeInsets.only(left:25.0, right: 5.2,top: 5,bottom: 5),
              width: double.infinity,
              child: Text("${authorObj.author}",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,letterSpacing: 1,fontWeight: FontWeight.w400
                  )),
            )
          ],
        ),
      ),
    );
  }
}
