
import 'package:flutter/material.dart';
import 'package:flutter_appentus_test_app/constants.dart';
import 'package:flutter_appentus_test_app/screens/author_list_screen.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GoogleMapWithCurrentLocationScreen extends StatefulWidget {
  static final String routName= 'GoogleMapWithCurrentLocationScreen';

  @override
  _GoogleMapWithCurrentLocationScreenState createState() => _GoogleMapWithCurrentLocationScreenState();
}
class _GoogleMapWithCurrentLocationScreenState extends State<GoogleMapWithCurrentLocationScreen> {

GoogleMapController googleMapController;

LatLng latlong=null;
CameraPosition _cameraPosition;
Set<Marker> _markers={};
final LatLng _center = const LatLng(45.521563, -122.677433);
TextEditingController locationController = TextEditingController();
String userName;

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    getusernameFromLocal();
    _cameraPosition=CameraPosition(target: LatLng(0, 0),zoom: 20.0);
    getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    appBar: AppBar(
        title: Text(userName!=null? userName:"HomeScreen"),
    ),
      body: Stack(
        children: [
          GoogleMap(
            onMapCreated: (GoogleMapController controller){
              googleMapController=(controller);
              googleMapController.animateCamera(
                  CameraUpdate.newCameraPosition(_cameraPosition));
            },
            markers:_markers ,
            onCameraIdle: (){
              setState(() {
              });
            }, initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 11.0,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              width: double.infinity,
              child: FlatButton(
                child: Text('Author List', style: TextStyle(fontSize: 16)),
                onPressed: () => {
                Navigator.pushNamed(context, AuthorListScreen.routName)
                },
                color: Colors.blue,
                textColor: Colors.white,
              ),
            ),
          )
        ],
      )


    );
  }


Future getCurrentLocation() async {
  LocationPermission permission = await Geolocator.checkPermission();
  if (permission != PermissionStatus.granted) {
    LocationPermission permission = await Geolocator.requestPermission();
    if (permission != PermissionStatus.granted)
      getLocation();
    return;
  }
  getLocation();
}
Future<void> getusernameFromLocal() async {
  var prefs = await SharedPreferences.getInstance();
  userName =  prefs.getString(username);
}

  getLocation() async {
       Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
       print(position.latitude);
       setState(() {
         latlong=new LatLng(position.latitude, position.longitude);
         _cameraPosition=CameraPosition(target:latlong,zoom: 12.0 );
         if(googleMapController!=null)
           googleMapController.animateCamera(
               CameraUpdate.newCameraPosition(_cameraPosition));
          _markers.add(Marker(markerId: MarkerId("a"),draggable:true,position: latlong,icon: BitmapDescriptor.defaultMarkerWithHue(
             BitmapDescriptor.hueBlue),onDragEnd:(_currentlatLng){
           latlong = _currentlatLng;
         }));
       });

     }


}
