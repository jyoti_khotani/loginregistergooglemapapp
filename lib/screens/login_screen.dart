import 'package:flutter/material.dart';
import 'package:flutter_appentus_test_app/database_helper/database_provider.dart';
import 'package:flutter_appentus_test_app/screens/google_map_with_current_location_screen.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../common.dart';
import '../constants.dart';

class LoginScreen extends StatefulWidget {
  static final String routName= 'LoginScreen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}
class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final databaseprovider = DataBaseprovider.createInstance();

      void _loginUser(){
         if (emailController.text.isEmpty) {
        Fluttertoast.showToast(msg: 'Please enter mobile email');
        return;
        } else if (!RegExp(
             r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
             .hasMatch(emailController.text)) {
           Fluttertoast.showToast(msg: 'Please enter correct email');
           return;
         }
         else if(passwordController.text.isEmpty){
           Fluttertoast.showToast(msg: 'Please enter password');
           return;
         }else{
           loginuserToLocal(emailController.text);
         }
      }

  Future<bool> loginuserToLocal( String userEmail) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      await databaseprovider.initializeDatabase();
      bool result=await databaseprovider.variantAlreadyPresent(userEmail);
      if(result){
        print("user logged in");
        Fluttertoast.showToast(msg: 'Logged in Successfuly');
        prefs.setString(useremail, userEmail);
        prefs.setBool(loginStatus, true);
        emailController.clear();
        passwordController.clear();
        Navigator.pushReplacementNamed(context, GoogleMapWithCurrentLocationScreen.routName);
      }else{
        Fluttertoast.showToast(msg: 'Logged in failed');
      }
      return true;
    } catch (Exception) {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(left: 15.0, right: 15.0),
          child: Column(
            children: [
              SizedBox(
                height: 200,
              ),
              Container(
                decoration: BoxDecoration(
                    color: lighBlueBackground,
                    borderRadius: BorderRadius.circular(15.0)
                ),
                child: TextField(
                  controller: emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      hintText: "Your email",
                      border: InputBorder.none,
                      hintStyle: sttyleText(
                          color: Colors.black38,weight: FontWeight.w400,size: 15)),
                ),
                padding: EdgeInsets.symmetric(horizontal: 10,),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                    color: lighBlueBackground,
                    borderRadius: BorderRadius.circular(15.0)
                ),
                child: TextField(
                  controller: passwordController,
                  decoration: InputDecoration(
                      hintText: "Your password",
                      border: InputBorder.none,
                      hintStyle: sttyleText(
                          color: Colors.black38,weight: FontWeight.w400,size: 15)),
                ),
                padding: EdgeInsets.symmetric(horizontal: 10,),
              ),
              SizedBox(
                height: 20,
              ),
              FlatButton(
                  onPressed: _loginUser,
                  child: Padding(
                    padding: const EdgeInsets.only(left:60.0,right: 60.0,top: 15.0,bottom: 15.0),
                    child: Text("Login",
                      style: sttyleText(color: Colors.white,
                          weight: FontWeight.w500,
                          size: 17),),
                  ),
                  splashColor: Colors.blueGrey,
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(
                          color: Colors.white
                      )
                  )
              )
            ],
          ),
        ),
      ),
    );
  }
}
