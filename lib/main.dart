import 'package:flutter/material.dart';
import 'package:flutter_appentus_test_app/screens/author_list_screen.dart';
import 'package:flutter_appentus_test_app/screens/dashborad_screen.dart';
import 'package:flutter_appentus_test_app/screens/home_screen.dart';
import 'package:flutter_appentus_test_app/screens/google_map_with_current_location_screen.dart';
import 'package:flutter_appentus_test_app/screens/login_screen.dart';
import 'package:flutter_appentus_test_app/screens/registration_screen.dart';
import 'package:flutter_appentus_test_app/view_model/author_list_view_model.dart';
import 'package:provider/provider.dart';
import 'common.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
        ChangeNotifierProvider(create: (ctx)=> AuthorListViewModel()),
        ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        routes: {
          "/": (context) => RegistrationScreen(),
          LoginScreen.routName: (context) => LoginScreen(),
          GoogleMapWithCurrentLocationScreen.routName: (context) => GoogleMapWithCurrentLocationScreen(),
          AuthorListScreen.routName: (context) => AuthorListScreen(),
        },
        initialRoute:  "/",
      ),
    );
      
      

  }
}

