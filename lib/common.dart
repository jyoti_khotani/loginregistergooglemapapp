import 'package:flutter/material.dart';

const kBackgroundColor = Color(0xFFFEFEFE);
const kTitleColor = Color(0xFF303030);
const kBodyTextColor = Color(0xFF4B4B4B);
const kTextLightColor = Color(0xFF959595);
final kShadowColor = Color(0xFFB7B7B7).withOpacity(0.16);
final kActiveShadowColo = Color(0xFF303030).withOpacity(0.26);
Color lighBlueBackground = Colors.blue.withOpacity(0.09);
final lighBlueforName = Color(0xFF4994E5);
const Color textBlackColor = Color(0XFF2D2D2D);

sttyleText(
    {double size = 17,
      Color color = textBlackColor,
      FontWeight weight = FontWeight.w500,
      double letterSpacing = 0.0}) =>
    TextStyle(
        fontSize: size,
        color: color,
        fontWeight: weight,
        letterSpacing: letterSpacing);


final boxDecoration = BoxDecoration(
  color: lighBlueBackground,
  borderRadius: BorderRadius.circular(15),

);

final boxDecorationWithShadow = BoxDecoration(
  color: lighBlueBackground,
  borderRadius: BorderRadius.circular(15),
  boxShadow: [
    BoxShadow(offset: Offset(5, 15), blurRadius: 25, color: kShadowColor)
  ],
);