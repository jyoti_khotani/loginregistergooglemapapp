import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DataBaseprovider{
  static DataBaseprovider _dataBaseprovider;
  static Database _database;

  String databasename = "appentus.db";
  String user_table = "user";
  String userid = "user_id";
  String username = "user_name";
  String useremail = "user_email";
  String usernumber = "user_number";
  String userpassword = "user_password";

  DataBaseprovider.createInstance();

  factory DataBaseprovider() {
    if (_dataBaseprovider != null) {
      _dataBaseprovider = DataBaseprovider.createInstance();
    }
    return _dataBaseprovider;
  }

  Future<Database> get database async{

    if(_database== null){
      _database = await initializeDatabase();
    }
    return _database;
  }

  Future<Database> initializeDatabase() async{
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + databasename;
    var ecommDatabase =
    await openDatabase(path, version: 1, onCreate: _createDB);
    return ecommDatabase;
  }

  void _createDB( Database db, int newVersion) async{
    await db.execute(
        'CREATE TABLE $user_table($userid INTEGER PRIMARY KEY AUTOINCREMENT, $username TEXT, $useremail TEXT, $usernumber TEXT, $userpassword TEXT)');
  }

  Future<int> insertUserToLocal(String userName, String userEmail, String number,String password) async{
    Database db = await this.database;
    Map<String, dynamic> user_map = {
      username: userName,
      useremail: userEmail,
      usernumber: number,
      userpassword: password
    };
    var result= await db.insert(user_table, user_map);
    return result;
  }

  Future<bool> variantAlreadyPresent(String userEmail) async {
    final db = await database;
    var result;
    result = await db
        .query(user_table, where: '$useremail = ?', whereArgs: [userEmail]);
    if (result.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

// write close database method





}