import 'dart:convert';

import 'package:flutter_appentus_test_app/service/web_api.dart';
import 'package:flutter_appentus_test_app/view_model/author.dart';
import 'package:http/http.dart' as http;

import '../constants.dart';


class WebApiImplemention implements WebApi {


  @override
  Future<List<Author>> fetchAllAuthorList() async {
  try{
    final response= await http.get(author_list);
    if(response.statusCode==200){
      final  results = jsonDecode(response.body);
      var jsonObject = results as List;
      return jsonObject.map((author) => Author.fromJson(author)).toList();
    }else{
      throw Exception("Unable to perform request!");
    }

  }catch(exception, s){
    throw("Unable to perform request! ${s}");
  }
  }

}