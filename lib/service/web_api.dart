import 'package:flutter_appentus_test_app/view_model/author.dart';

abstract class WebApi{

  Future<List<Author>> fetchAllAuthorList();
}